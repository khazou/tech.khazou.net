This repository hosts my technical blog.

Everything on it is under the CC-Zero licence. No strings attached, no liabilities nor warranty of any kind.