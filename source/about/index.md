---
title: About me
date: 2020-09-23 11:24:38
comments: false
---

My name is Antoine Lenoir and I'm a French ERP Specialist and Software Developer living in Switzerland.

I'm mostly specialized and will talk a lot about the following topics:

* Abacus ERP
* Netsuite ERP
* Ruby and Rails development

I hope you'll enjoy your visit here!