---
title: Netsuite Saved Searches snippets
date: 2020-09-23 18:00:00
tags:
- Netsuite
- Saved Searches
categories:
- Netsuite
---

{% asset_img "database.jpg" "Nice old school database" %}

Saved searches are a really useful feature in Netsuite, and I use them daily in my job. Here are some "Quality-of-Life" snippets I have discovered, or used in the last years.

## Get the ID of a List value

Most of the times, we like to return the value of a List or Record field in a saved search, but what should we do when we need to return an actual ID? (For example when you want to integrate with another app through the saved search).

For a linked record, the solution is quite easy. Just navigate in the field list to __Linked Record Fields...__, choose __Internal ID__ in the dropdown, and done.

When you want to link to a list, it is a bit more complex. (But not that complex, actually). Just define your column type as __Formula (Numeric)__, and open the __Edit formula__ window.

In the __Fields__ dropdown, choose the List field you want to return the id of (_In my example, the List is an Item Category list, with the Field ID_ custitem_itemcategory). The formula should look like the following code snippet.

```
{custitem_itemcategory}
```

To return the ID of the field, you need to explicitely ask Netsuite to return the ID, so add <code>.id</code> at the end of the field name, so your formula looks like that:

```
{custitem_itemcategory.id}
```

## Format the dates in a saved search

When exporting data in CSV, I usually need to format the dates in a specific way. (I'm living in Switzerland, where the standard date format is DD.MM.YYYY, but I need to be able to communicate with countries where this format is not natural).

For that, Netsuite provides the <code>TO_CHAR</code> function in the Formula builder, which is used as follows:

``` sql
TO_CHAR({my_date_field}, 'date_format')
```

A lot of information about the <code>TO_CHAR</code> function is available on the [documentation of the Oracle Database](https://docs.oracle.com/cd/B28359_01/server.111/b28286/sql_elements004.htm#SQLRF00212).

I hope these small snippets can help you solve some issues in your saved search creation. Enjoy!

_<span>Photo by <a href="https://unsplash.com/@jankolar?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Jan Antonin Kolar</a> on <a href="https://unsplash.com/s/photos/database?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>_