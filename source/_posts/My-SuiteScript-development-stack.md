---
title: My SuiteScript development stack
date: 2020-09-29 10:49:07
tags:
- Netsuite
- SuiteScript
categories:
- Netsuite
---

{% asset_img "javascript.jpg" "Lovin' Scriptin'" %}

When it comes to SuiteScript development, many options are available in terms of environment and editor of choice. Netsuite offers a full development environment based on Eclipse, named [SuiteCloud IDE](https://www.netsuite.com/portal/platform/developer/suitecloud-ide.shtml). When I began developing SuiteScripts, I naturally used it as it was recommended by the official Netsuite Learning Material.

It made me remember why I *hated* Eclipse. (Small disclaimer here. I can completely understand that people like Eclipse, but I don't. The following rant is just based on my personal experience and should not be taken as-is.) I think it is slow, bloated, and a sad reminder of what IDEs used to be in the 2000's. Having to use a full IDE to develop scripts is in my opinion like having to use a chainsaw to open a tin can. Overkill.

Thus, I moved on to my editor of choice.

## From an IDE to a Text Editor : Visual Studio Code

I like Visual Studio Code. I will not deny that. I like the fact that it is multi-platform, extensible, light (compared to full-fledged IDEs). I like its UI. Well, let's just say I like it.

Of course, as a text editor, VSCode does not embed natively some features that SuiteCloud IDE provides, like the connectivity between the IDE and Netsuite's File Cabinet. Well, it does not bother me to upload files through the UI, especially when doing some development. In the company I'm working for, we're two SuiteScripts developers, and we do not work on the same projects, nor scripts.

That being said, one of the major perks of VSCode is the extensions catalog, where you can find a lot of stuff, including Netsuite-related extensions.

## SuiteScript snippet extension : GVO Snippets

I will lie if I say I'm using a lot of extensions to script Netsuite. The major one is [GVO Snippets](https://marketplace.visualstudio.com/items?itemName=kodybrand.gvo-ss-snippets) which includes the script skeletons. (Not having to type the whole define sequence for a script is a peach)

The autocompletion sequence is automatically suggested when editing a Javascript file, and is relatively easy to use.

{% asset_img "autocomplete.png" "Autocompletion" %}

{% asset_img "autocomplete2.png" "Autocompletion Done." %}

## Source control and backups : Git 

To backup everything I script (because no computer is completely failsafe), all my scripts are located in a private git repository on GitLab. Two big advantages to use git to backup and control the source code:

* If I need to edit a script in a rush (Yes, it happens) and I don't have access to my computer at a given time, I can still do it in GitLab's UI (and the changes will be committed to the repository). If I edit the script in Netsuite's UI, it won't be synced, and I'll have to update the changes in the repo as well.
* I can always fetch a previous version of the script if I botched the modifications (Yes, it also happens)

Another perk of using Git is that it is natively included in VSCode, and I can switch between branches, commit, push directly from the UI.

That's all for today! Stay safe, and do not forget to save your work. Twice!

_<span>Photo by <a href="https://unsplash.com/@maxchen2k?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Max Chen</a> on <a href="https://unsplash.com/s/photos/javascript?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>_