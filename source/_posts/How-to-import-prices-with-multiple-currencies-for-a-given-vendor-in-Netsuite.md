---
title: How to import prices with multiple currencies for a given vendor in Netsuite?
date: 2020-09-25 09:00:00
categories:
- Netsuite
tags:
- Netsuite
- CSV imports
- OneWorld
---

{% asset_img "csv.jpg" "I love these CSV files" %}

CSV imports in Netsuite are my personal preference to create or update great loads of data. One could argue that not all data is importable through that way, but for my daily job, it clearly does everything I need.

## The issue

One slight inconvenience I encountered though, is that it is impossible to import the vendor prices for multiple currencies for a given vendor.

Let me explain the context. We are purchasing items from a vendor, which we'll call __Company A__. We purchase some of these items in __EUR__ and others in __USD__. __Company A__ mostly deals with us in __EUR__, so the primary currency is __EUR__. When we import the vendor prices, we do it generally for all the items at once, and not vendor by vendor, so we are using the __Items &gt; Inventory Items__ CSV import, and we map the __Item Vendors__ sublist.

In that mapping, there is no mapping for the currency, and as such, we can not import the prices for all the items at once. The mapping always uses the primary currency of __Company A__, and I can't import my USD prices.

## The "solution" - More like an exploit...

Until Netsuite updates its Inventory Item mapping, I'll have to use the following exploit:

1. Change the primary currency of __Company A__ to __USD__.
1. Import the vendor prices for all the __USD__ items of __Company A__ via a CSV import.
1. Revert the primary currency of __Company A__ to __EUR__.
1. Import all my other prices in bulk.

This looks more like an exploit (it is actually) more than a solution, but that is (at least for now) a working exploit.

_<span>Photo by <a href="https://unsplash.com/@mbaumi?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Mika Baumeister</a> on <a href="https://unsplash.com/s/photos/csv?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>_