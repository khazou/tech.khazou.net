---
title: 'puts "Hello World"'
date: 2020-09-23 12:00:00
---
![Hello World!](/images/helloWorldSplash.jpg)

Here we go again. Another try at blogging for me, about tech-related stuff, mostly ERP and Software development (which is the kind of stuff I'm working on).

I hope you'll enjoy your time here, and that I can provide you with some solutions for your issues.

<span>Photo by <a href="https://unsplash.com/@lucabravo?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Luca Bravo</a> on <a href="https://unsplash.com/s/photos/ruby-code?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>